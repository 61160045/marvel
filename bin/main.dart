import 'dart:io';
import 'rungame.dart';

void main(List<String> arguments) {
  runGame();
}

void runGame() {
  bool loop = true;
  print('======================================'
      '\nWelcome to Marvel Fighter Game'
      '\n======================================'
      '\nPlease Select your hero'
      '\n======================================'
      '\n1.Ironman'
      '\n2.Captain America'
      '\n3.Thor'
      '\n4.Hawkeye'
      '\n5.Black Widow'
      '\n6.Spider Man'
      '\n7.Hulk'
      '\n8.Dr.Stange'
      '\n======================================');
  int? selecthero = int.parse(stdin.readLineSync()!);
  print('======================================'
      '\nPlease Select hero bot'
      '\n======================================'
      '\n1.Ironman'
      '\n2.Captain America'
      '\n3.Thor'
      '\n4.Hawkeye'
      '\n5.Black Widow'
      '\n6.Spider Man'
      '\n7.Hulk'
      '\n8.Dr.Stange'
      '\n======================================');
  int? selectbot = int.parse(stdin.readLineSync()!);

  final rungame game = new rungame();
  game.selectHero(selecthero);
  game.selectbot(selectbot);
  while (loop) {
    game.action();
    game.randombotAction();
    loop = game.checkHp();
  }
}

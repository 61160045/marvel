abstract class setgame {
  int turn = 0;
  var player;
  var bot;

  void selectHero(int selecthero);
  void showWinnter();
  void selectbot(int selectbot);
  void action();
  void randombotAction();
  void attack();
  void defend();
  void botAtack();
  void botDefend();
  void playerwasAttack(double boatk);
  void botwasAttack(double atk);
  void playerCritical();
  bool checkHp();
  void showBotWinner();
  void botCritical();
}

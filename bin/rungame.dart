import 'bot/botblackwidow.dart';
import 'bot/botcaptainamerica.dart';
import 'bot/botdrstrange.dart';
import 'bot/bothawkeye.dart';
import 'bot/bothulk.dart';
import 'bot/botironman.dart';
import 'bot/botspiderman.dart';
import 'bot/botthor.dart';
import 'hero/blackwidow.dart';
import 'hero/captainamerica.dart';
import 'hero/drstrange.dart';
import 'hero/hawkeye.dart';
import 'hero/hulk.dart';
import 'hero/ironman.dart';
import 'setgame.dart';
import 'hero/spiderman.dart';
import 'hero/thor.dart';
import 'dart:io';
import 'dart:math';

class rungame extends setgame
    with
        ironman,
        captainamerica,
        thor,
        hawkeye,
        blackwidow,
        spiderman,
        drstrange,
        hulk,
        botironman,
        botcaptainamerica,
        botblackwidow,
        botdrstrange,
        bothulk,
        botspiderman,
        bothawkeye,
        botthor {
  double hp = 0;
  double bothp = 0;
  int turn = 1;

  @override
  void selectHero(int select) {
    if (select == 1) {
      player = ironman;
      hp = iron_hp;
      print('Your hero : ' + callIron());
    } else if (select == 2) {
      player = captainamerica;
      hp = cap_hp;
      print('Your hero : ' + callCap());
    } else if (select == 3) {
      player = thor;
      hp = th_hp;
      print('Your hero : ' + callThor());
    } else if (select == 4) {
      player = hawkeye;
      hp = he_hp;
      print('Your hero : ' + callHawkeye());
    } else if (select == 5) {
      player = blackwidow;
      hp = bw_hp;
      print('Your hero : ' + callBlackWi());
    } else if (select == 6) {
      player = spiderman;
      hp = sm_hp;
      print('Your hero : ' + callSpi());
    } else if (select == 7) {
      player = hulk;
      hp = hk_hp;
      print('Your hero : ' + callHulk());
    } else if (select == 8) {
      player = drstrange;
      hp = ds_hp;
      print('Your hero : ' + callDs());
    } else {
      print('Select hero failed. Please select hero again');
    }
  }

  @override
  void selectbot(int selectbot) {
    if (selectbot == 1) {
      bot = botironman;
      bothp = biron_hp;
      print('Your bot : ' + callBotIron());
    } else if (selectbot == 2) {
      bot = botcaptainamerica;
      bothp = botcap_hp;
      print('Your bot : ' + callbotCap());
    } else if (selectbot == 3) {
      bot = botthor;
      bothp = botth_hp;
      print('Your bot : ' + callbotThor());
    } else if (selectbot == 4) {
      bot = bothawkeye;
      bothp = bothe_hp;
      print('Your bot : ' + callbotHawkeye());
    } else if (selectbot == 5) {
      bot = botblackwidow;
      bothp = botbw_hp;
      print('Your bot : ' + callbotBlackWi());
    } else if (selectbot == 6) {
      bot = botspiderman;
      bothp = botsm_hp;
      print('Your bot : ' + callbotSpi());
    } else if (selectbot == 7) {
      bot = bothulk;
      bothp = bothk_hp;
      print('Your bot : ' + callbotHulk());
    } else if (selectbot == 8) {
      bot = botdrstrange;
      bothp = botds_hp;
      print('Your bot : ' + callbotDs());
    } else {
      print('Select bot failed. Please select bot again');
    }
    print('======================================'
        '\nWelcome to fight stage');
  }

  @override
  void action() {
    while (true) {
      print('================ Turn $turn =============='
          '\nYOUR TURN'
          '\nSelect your action'
          '\n1.Attack'
          '\n2.Defend'
          '\n3.Do nothing'
          '\n======================================');
      int? action = int.parse(stdin.readLineSync()!);
      turn++;
      if (action == 1) {
        print('Your action is : Attack');
        print(showAttack());
        attack();
        break;
      } else if (action == 2) {
        print('Your action is : Defend');
        print(showDefend());
        defend();
        break;
      } else if (action == 3) {
        print('Your action is : Do nothing.'
            '\nCritical triggered');
        playerCritical();
        break;
      }
    }
  }

  @override
  void randombotAction() {
    Random randomnum = new Random();
    int number = randomnum.nextInt(100);
    if (number % 2 == 0) {
      botDefend();
      botCritical();
    } else {
      botAtack();
    }
  }

  @override
  void attack() {
    if (player == ironman) {
      print('Iron man Attack'
          '\nIron Blaster');
      botwasAttack(getIronmanAtk());
    } else if (player == captainamerica) {
      print('Captain America Attack'
          '\nThrow a shield');
      botwasAttack(getCapAtk());
    } else if (player == thor) {
      print('Thor Attack'
          '\nThunderbolt!');
      botwasAttack(getThorAtk());
    } else if (player == hawkeye) {
      print('Hawkeye Attack'
          '\nGot hit!');
      botwasAttack(getHawkAtk());
    } else if (player == blackwidow) {
      print('Black Widow Attack'
          '\nHook down!');
      botwasAttack(getBlackWiAtk());
    } else if (player == spiderman) {
      print('Spiderman Attack'
          '\nStop it!');
      botwasAttack(getSpiAtk());
    } else if (player == hulk) {
      print('Hulk Attack'
          '\nHu Arghhhh!');
      botwasAttack(getHulkAtk());
    } else if (player == drstrange) {
      print('Dr.Strange Attack'
          '\nDont flout a magic');
      botwasAttack(getDsAtk());
    } else {
      print('Select hero failed. Please select hero again');
    }
  }

  @override
  void botAtack() {
    print('======================================'
        '\nBOT TURN');
    print('BOT ATTACK');
    if (bot == botironman) {
      print('Iron man bot Attack.'
          '\nIron Blaster');
      playerwasAttack(getBotIronmanAtk());
    } else if (bot == botcaptainamerica) {
      print('Bot Captain America Attack.'
          '\nThrow a shield!');
      playerwasAttack(getbotCapAtk());
    } else if (bot == botthor) {
      print('Bot Thor Attack.'
          '\nThunderbolt!!');
      playerwasAttack(getbotThorAtk());
    } else if (bot == bothawkeye) {
      print('Bot Hawkeye Attack'
          '\nGot hit!');
      playerwasAttack(getbotHawkAtk());
    } else if (bot == botblackwidow) {
      print('Bot Black Widow Attack'
          '\nHook Down!');
      playerwasAttack(getbotBlackWiAtk());
    } else if (bot == botspiderman) {
      print('Spiderman Attack'
          '\nStop it!');
      playerwasAttack(getbotSpiAtk());
    } else if (bot == bothulk) {
      print('Hulk Attack'
          '\nHu Arghhhh!');
      playerwasAttack(getbotHulkAtk());
    } else if (bot == botdrstrange) {
      print('Dr.Strange Attack'
          '\nDont flout a magic.');
      playerwasAttack(getbotDsAtk());
    } else {
      print('bot atk failed');
    }
  }

  @override
  void defend() {
    double def = 0;
    if (player == ironman) {
      print('Iron man was received DEF+10');
      def = ironDef();
      print('Iron Sheild');
      print('IRON MAN HP : $iron_hp');
      print('IRON MAN DEF : $iron_def');
    } else if (player == captainamerica) {
      print('Captain America was received DEF+50');
      def = capDef();
      print('Got sheild!');
      print('CAPTAIN AMERICA HP : $cap_hp');
      print('CAPTAIN AMERICA DEF : $cap_def');
    } else if (player == thor) {
      print('Thor was received DEF+5');
      def = thorDef();
      print('Super Charge');
      print('THOR HP : $th_hp');
      print('THOR DEF : $th_def');
    } else if (player == hawkeye) {
      print('Hawkeye was received DEF+5');
      def = hawkDef();
      print('Protection');
      print('HAWKEYE HP : $he_hp');
      print('HAWKEYE DEF : $he_def');
    } else if (player == blackwidow) {
      print('Black Widow was received DEF+10');
      def = blackDef();
      print('COUNTER ATTACK');
      print('BLACK WIDOW HP : $bw_hp');
      print('BLACK WIDOW DEF : $bw_def');
    } else if (player == spiderman) {
      print('Spiderman was received DEF+10');
      def = spiDef();
      print('Moving out!');
      print('SPIDERMAN HP : $sm_hp');
      print('SPIDERMAN DEF : $sm_def');
    } else if (player == hulk) {
      print('Hulk was received DEF+10');
      def = hulkDef();
      print('Hulahhhhh!');
      print('HULK HP : $hk_hp');
      print('HULK DEF : $hk_def');
    } else if (player == drstrange) {
      print('Dr.Strange was received DEF+25');
      def = dsDef();
      print('Glassess!');
      print('DR.STRANGE HP : $ds_hp');
      print('DR.STRANGE DEF : $ds_def');
    } else {
      print('bot defend failed');
    }
  }

  @override
  void botDefend() {
    print('======================================'
        '\nBOT TURN');
    print('BOT DEFEND');
    print('Critical triggered');
    double def = 0;
    if (bot == botironman) {
      print('Iron man bot was received DEF+10');
      def = botIronDef();
      print('Iron Sheild');
      print('BOT IRON MAN HP : $biron_hp');
      print('BOT IRON MAN DEF : $biron_def');
    } else if (bot == botcaptainamerica) {
      print('Captain America bot was received DEF+50');
      def = botcapDef();
      print('Got sheild!');
      print('BOT CAPTAIN AMERICA HP : $botcap_hp');
      print('BOT CAPTAIN AMERICA DEF : $botcap_def');
    } else if (bot == botthor) {
      print('Thor bot was received DEF+5');
      def = botThorDef();
      print('Super Charge');
      print('BOT THOR HP : $botth_hp');
      print('BOT THOR DEF : $botth_def');
    } else if (bot == bothawkeye) {
      print('Hawkeye bot was received DEF+5');
      def = bothawkDef();
      print('Protection');
      print('BOT HAWKEYE HP : $bothe_hp');
      print('BOT HAWKEYE DEF : $bothe_def');
    } else if (bot == botblackwidow) {
      print('Black Widow bot was received DEF+10');
      def = botblackDef();
      print('COUNTER ATTACK');
      print('BOT BLACK WIDOW HP : $botbw_hp');
      print('BOT BLACK WIDOW DEF : $botbw_def');
    } else if (bot == botspiderman) {
      print('Spiderman bot was received DEF+10');
      def = botspiDef();
      print('Moving out!');
      print('BOT SPIDERMAN HP : $botsm_hp');
      print('BOT SPIDERMAN DEF : $botsm_def');
    } else if (bot == bothulk) {
      print('Hulk bot was received DEF+10');
      def = bothulkDef();
      print('Hulahhhhh!');
      print('BOT HULK HP : $bothk_hp');
      print('BOT HULK DEF : $bothk_def');
    } else if (bot == botdrstrange) {
      print('Dr.Strange bot was received DEF+25');
      def = botdsDef();
      print('Glassess!');
      print('BOT DR.STRANGE HP : $botds_hp');
      print('BOT DR.STRANGE DEF : $botds_def');
    } else {
      print('bot defend failed');
    }
  }

  @override
  void playerwasAttack(double botatk) {
    if (player == ironman) {
      ironwasAtk(checkIronDef(botatk));
      hp = getHpIron();
      print('Damage :  $botatk');
      print('Ironman took a damage.'
          '\nIron man HP : $hp');
    } else if (player == captainamerica) {
      capwasAtk(checkCapDef(botatk));
      hp = getHpCap();
      print('Damage :  $botatk');
      print('Captain America took a damage.'
          '\nCaptain Ameica HP : $hp');
    } else if (player == thor) {
      thorwasAtk(checkThorDef(botatk));
      hp = getHpThor();
      print('Damage :  $botatk');
      print('Thor took a damage.'
          '\nThor HP : $hp');
    } else if (player == hawkeye) {
      hawkwasAtk(checkHawkDef(botatk));
      hp = getHpHawk();
      print('Damage :  $botatk');
      print('Hawkeye took a damage.'
          '\nHawkeye HP : $hp');
    } else if (player == blackwidow) {
      blackwasAtk(checkBlackDef(botatk));
      hp = getHpBlack();
      print('Damage :  $botatk');
      print('Black Widow took a damage.'
          '\nBlack Widow HP : $hp');
    } else if (player == spiderman) {
      spiwasAtk(checkSpiDef(botatk));
      hp = getHpSpi();
      print('Damage :  $botatk');
      print('Spiderman took a damage.'
          '\nSpiderman HP : $hp');
    } else if (player == hulk) {
      hulkwasAtk(checkHulkDef(botatk));
      hp = getHpHulk();
      print('Damage :  $botatk');
      print('Hulk took a damage.'
          '\nHulk HP : $hp');
    } else if (player == drstrange) {
      dswasAtk(checkDsDef(botatk));
      hp = getHpDs();
      print('Damage :  $botatk');
      print('Dr.Strange took a damage.'
          '\nDr.Strange HP : $hp');
    } else {
      print('attack failed');
    }
  }

  @override
  void botwasAttack(double atk) {
    if (bot == botironman) {
      botIronwasAtk(checkbotIronDef(atk));
      bothp = getHpBotIron();
      print('Damage :  $atk');
      print('Iron man bot took a damage.'
          '\nIron man bot HP : $bothp');
    } else if (bot == botcaptainamerica) {
      botcapwasAtk(checkbotCapDef(atk));
      bothp = getHpbotCap();
      print('Damage :  $atk');
      print('Captain America bot took a damage.'
          '\nCaptain America bot HP : $bothp');
    } else if (bot == botthor) {
      botthorwasAtk(checkbotThorDef(atk));
      bothp = getHpbotThor();
      print('Damage :  $atk');
      print('Thor bot took a damage.'
          '\nThor bot HP : $bothp');
    } else if (bot == bothawkeye) {
      bothawkwasAtk(checkbotHawkDef(atk));
      bothp = getHpbotHawk();
      print('Damage :  $atk');
      print('Hawkeye bot took a damage.'
          '\nHawkeye bot HP : $bothp');
    } else if (bot == botblackwidow) {
      botblackwasAtk(checkbotBlackDef(atk));
      bothp = getHpbotBlack();
      print('Damage :  $atk');
      print('Black Widow bot took a damage.'
          '\nBlack Widow bot HP : $bothp');
    } else if (bot == botspiderman) {
      botspiwasAtk(checkbotSpiDef(atk));
      bothp = getHpbotSpi();
      print('Damage :  $atk');
      print('Spiderman bot took a damage.'
          '\nSpiderman bot HP : $bothp');
    } else if (bot == bothulk) {
      bothulkwasAtk(checkbotHulkDef(atk));
      bothp = getHpbotHulk();
      print('Damage :  $atk');
      print('Hulk bot took a damage.'
          '\nHulk bot HP : $bothp');
    } else if (bot == botdrstrange) {
      botDswasAtk(checkbotDsDef(atk));
      bothp = getHpbotDs();
      print('Damage :  $atk');
      print('Dr.Strange bot took a damage.'
          '\nDr.Strange bot HP : $bothp');
    } else {
      print('bot attack failed');
    }
  }

  @override
  void playerCritical() {
    if (player == ironman) {
      criIron();
      print('IRON MAN HP : $iron_hp');
      print('IRON MAN ATK : $iron_atk');
      print('IRON MAN DEF : $iron_def');
    } else if (player == captainamerica) {
      criCap();
      print('CAPTAIN AMERICA HP : $cap_hp');
      print('CAPTAIN AMERICA ATK : $cap_atk');
      print('CAPTAIN AMERICA DEF : $cap_def');
    } else if (player == thor) {
      criThor();
      print('THOR HP : $th_hp');
      print('THOR ATK : $th_atk');
      print('THOR DEF : $th_def');
    } else if (player == hawkeye) {
      criHawk();
      print('HAWKEYE HP : $he_hp');
      print('HAWKEYE ATK : $he_atk');
      print('HAWKEYE DEF : $he_def');
    } else if (player == blackwidow) {
      criBlack();
      print('BLACK WIDOW HP : $bw_hp');
      print('BLACK WIDOW ATK : $bw_atk');
      print('BLACK WIDOW DEF : $bw_def');
    } else if (player == spiderman) {
      criSpi();
      print('SPIDERMAN HP : $sm_hp');
      print('SPIDERMAN ATK : $sm_atk');
      print('SPIDERMAN DEF : $sm_def');
    } else if (player == hulk) {
      criHulk();
      print('HULK HP : $hk_hp');
      print('HULK HP : $hk_atk');
      print('HULK DEF : $hk_def');
    } else if (player == drstrange) {
      criDs();
      print('DR.STRANGE HP : $ds_hp');
      print('DR.STRANGE HP : $ds_atk');
      print('DR.STRANGE DEF : $ds_def');
    } else {
      print('critical failed.');
    }
  }

  @override
  void botCritical() {
    if (bot == botironman) {
      cribotIron();
      print('BOT IRON MAN DAMAGE : $biron_atk');
    } else if (bot == botcaptainamerica) {
      cribotCap();
      print('BOT CAPTAIN AMERICA DAMAGE : $botcap_atk');
    } else if (bot == botthor) {
      cribotThor();
      print('BOT THOR DAMAGE : $botth_atk');
    } else if (bot == bothawkeye) {
      cribotHawk();
      print('BOT HAWKEYE DAMAGE : $bothe_atk');
    } else if (bot == botblackwidow) {
      cribotBlack();
      print('BOT BLACK WIDOW DAMAGE : $botbw_atk');
    } else if (bot == botspiderman) {
      cribotSpi();
      print('BOT SPIDERMAN DAMAGE : $botsm_atk');
    } else if (bot == bothulk) {
      cribotHulk();
      print('BOT HULK DAMAGE : $bothk_atk');
    } else if (bot == botdrstrange) {
      cribotDs();
      print('BOT DR.STRANGE DAMAGE : $botds_atk');
    } else {
      print('Critical bot failed.');
    }
  }

  @override
  bool checkHp() {
    if (hp <= 0) {
      showBotWinner();
      return false;
    }
    if (bothp <= 0) {
      showWinnter();
      return false;
    }
    return true;
  }

  @override
  void showBotWinner() {
    print('======================================'
        '\nBot is winner!!'
        '\nPlay as $bot'
        '\n======================================');
  }

  @override
  void showWinnter() {
    print('======================================'
        '\nYour are winner!!'
        '\nPlay as : $player'
        '\n======================================');
  }

  String showAttack() {
    return '          >_________________________________'
        '\n[########[]_________________________________ >'
        '\n          >';
  }

  String showDefend() {
    return '       _ _ _ _ _ _ _ _ _     '
        '\n      |       | |       |'
        '\n      |       | |       |'
        '\n      |       | |       |'
        '\n      |_______| |_______|'
        '\n      |_______   _______|'
        '\n      |       | |       |'
        '\n      |       | |       |'
        '\n      |       | |       |'
        '\n      |       | |       |'
        '\n      |_ _ _ _| | _ __ _|';
  }
}

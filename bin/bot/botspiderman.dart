mixin botspiderman {
  double botsm_hp = 450;
  double botsm_atk = 110;
  double botsm_def = 90;

  double getHpbotSpi() {
    return botsm_hp;
  }

  String callbotSpi() {
    return 'hi! I am Spider man.';
  }

  void botspiwasAtk(double botatk) {
    this.botsm_hp -= botatk;
  }

  double getbotSpiAtk() {
    return botsm_atk;
  }

  double checkbotSpiDef(double atk) {
    if (botsm_def - atk < 0) {
      return atk - botsm_def;
    }
    return 0;
  }

  double botspiDef() {
    return botsm_def += 20;
  }

  double cribotSpi() {
    return botsm_atk = botsm_atk * 1.3;
  }
}

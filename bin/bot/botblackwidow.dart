mixin botblackwidow {
  double botbw_hp = 450;
  double botbw_atk = 150;
  double botbw_def = 120;

  double getHpbotBlack() {
    return botbw_hp;
  }

  String callbotBlackWi() {
    return 'They are called me Black widow.';
  }

  void botblackwasAtk(double botatk) {
    this.botbw_hp -= botatk;
  }

  double getbotBlackWiAtk() {
    return botbw_atk;
  }

  double checkbotBlackDef(double atk) {
    if (botbw_def - atk < 0) {
      return atk - botbw_def;
    }
    return 0;
  }

  double botblackDef() {
    return botbw_def = botbw_def + 10;
  }

  double cribotBlack() {
    return botbw_atk = botbw_atk * 1.3;
  }
}

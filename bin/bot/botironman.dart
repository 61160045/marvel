mixin botironman {
  double biron_hp = 450;
  double biron_atk = 120;
  double biron_def = 90;

  double getHpBotIron() {
    return biron_hp;
  }

  void setHpBotIron(double newhp) {
    this.biron_hp = newhp;
  }

  String callBotIron() {
    return 'I am Iron man';
  }

  void botIronwasAtk(double atk) {
    this.biron_hp = biron_hp - atk;
  }

  double checkbotIronDef(double atk) {
    if (biron_def - atk < 0) {
      return atk - biron_def;
    }
    return 0;
  }

  double getBotIronmanAtk() {
    return biron_atk;
  }

  double botIronDef() {
    return biron_def += 10;
  }

  double cribotIron() {
    return biron_atk = biron_atk * 1.3;
  }
}

mixin bothulk {
  double bothk_hp = 500;
  double bothk_atk = 100;
  double bothk_def = 110;

  double getHpbotHulk() {
    return bothk_hp;
  }

  String callbotHulk() {
    return 'Like a hulk.';
  }

  void bothulkwasAtk(double botatk) {
    this.bothk_hp -= botatk;
  }

  double getbotHulkAtk() {
    return bothk_atk;
  }

  double checkbotHulkDef(double atk) {
    if (bothk_def - atk < 0) {
      return atk - bothk_def;
    }
    return 0;
  }

  double bothulkDef() {
    return bothk_def = bothk_def + 15;
  }

  double cribotHulk() {
    return bothk_atk = bothk_atk * 1.3;
  }
}

mixin bothawkeye {
  double bothe_hp = 450;
  double bothe_atk = 110;
  double bothe_def = 50;

  double getHpbotHawk() {
    return bothe_hp;
  }

  String callbotHawkeye() {
    return 'Remember me ?I am Hawkeye.';
  }

  void bothawkwasAtk(double botatk) {
    this.bothe_hp -= botatk;
  }

  double getbotHawkAtk() {
    return bothe_atk;
  }

  double checkbotHawkDef(double atk) {
    if (bothe_def - atk < 0) {
      return atk - bothe_def;
    }
    return 0;
  }

  double bothawkDef() {
    return bothe_def = bothe_def + 5;
  }

  double cribotHawk() {
    return bothe_atk = bothe_atk * 1.3;
  }
}

mixin botthor {
  double botth_hp = 500;
  double botth_atk = 110;
  double botth_def = 90;

  double getHpbotThor() {
    return botth_hp;
  }

  String callbotThor() {
    return 'I am thor son of odin.';
  }

  void botthorwasAtk(double botatk) {
    this.botth_hp -= botatk;
  }

  double getbotThorAtk() {
    return botth_atk;
  }

  double checkbotThorDef(double atk) {
    if (botth_def - atk < 0) {
      return atk - botth_def;
    }
    return 0;
  }

  double botThorDef() {
    return botth_def += 5;
  }

  double cribotThor() {
    return botth_atk = botth_atk * 1.3;
  }
}

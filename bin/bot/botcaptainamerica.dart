mixin botcaptainamerica {
  double botcap_hp = 450;
  double botcap_atk = 110;
  double botcap_def = 100;

  double getHpbotCap() {
    return botcap_hp;
  }

  String callbotCap() {
    return 'I am Captain America';
  }

  void botcapwasAtk(double botatk) {
    this.botcap_hp -= botatk;
  }

  double getbotCapAtk() {
    return botcap_atk;
  }

  double checkbotCapDef(double atk) {
    if (botcap_def - atk < 0) {
      return atk - botcap_def;
    }
    return 0;
  }

  double botcapDef() {
    return botcap_def = botcap_def + 50;
  }

  double cribotCap() {
    return botcap_atk = botcap_atk * 1.3;
  }
}

mixin botdrstrange {
  double botds_hp = 450;
  double botds_atk = 100;
  double botds_def = 80;

  double getHpbotDs() {
    return botds_hp;
  }

  String callbotDs() {
    return 'My name is Steven Strange.';
  }

  void botDswasAtk(double botatk) {
    this.botds_hp -= botatk;
  }

  double getbotDsAtk() {
    return botds_atk;
  }

  double checkbotDsDef(double atk) {
    if (botds_def - atk < 0) {
      return atk - botds_def;
    }
    return 0;
  }

  double botdsDef() {
    return botds_def = botds_def + 25;
  }

  double cribotDs() {
    return botds_atk = botds_atk * 1.5;
  }
}

mixin captainamerica {
  double cap_hp = 450;
  double cap_atk = 110;
  double cap_def = 100;

  double getHpCap() {
    return cap_hp;
  }

  String callCap() {
    return 'I am Captain America';
  }

  void capwasAtk(double botatk) {
    this.cap_hp -= botatk;
  }

  double getCapAtk() {
    return cap_atk;
  }

  double checkCapDef(double atk) {
    if (cap_def - atk < 0) {
      return atk - cap_def;
    }
    return 0;
  }

  double capDef() {
    return cap_def = cap_def + 50;
  }

  double criCap() {
    return cap_atk = cap_atk * 1.3;
  }
}

mixin thor {
  double th_hp = 500;
  double th_atk = 110;
  double th_def = 90;

  double getHpThor() {
    return th_hp;
  }

  String callThor() {
    return 'I am thor son of odin.';
  }

  void thorwasAtk(double botatk) {
    this.th_hp -= botatk;
  }

  double getThorAtk() {
    return th_atk;
  }

  double checkThorDef(double atk) {
    if (th_def - atk < 0) {
      return atk - th_def;
    }
    return 0;
  }

  double thorDef() {
    return th_def += 5;
  }

  double criThor() {
    return th_atk = th_atk * 1.3;
  }
}

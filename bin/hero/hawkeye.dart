mixin hawkeye {
  double he_hp = 450;
  double he_atk = 110;
  double he_def = 50;

  double getHpHawk() {
    return he_hp;
  }

  String callHawkeye() {
    return 'Remember me ?I am Hawkeye.';
  }

  void hawkwasAtk(double botatk) {
    this.he_hp -= botatk;
  }

  double getHawkAtk() {
    return he_atk;
  }

  double checkHawkDef(double atk) {
    if (he_def - atk < 0) {
      return atk - he_def;
    }
    return 0;
  }

  double hawkDef() {
    return he_def += 30;
  }

  double criHawk() {
    return he_atk = he_atk * 1.3;
  }
}

mixin drstrange {
  double ds_hp = 450;
  double ds_atk = 100;
  double ds_def = 80;

  double getHpDs() {
    return ds_hp;
  }

  String callDs() {
    return 'My name is Steven Strange.';
  }

  void dswasAtk(double botatk) {
    this.ds_hp -= botatk;
  }

  double getDsAtk() {
    return ds_atk;
  }

  double checkDsDef(double atk) {
    if (ds_def - atk < 0) {
      return atk - ds_def;
    }
    return 0;
  }

  double dsDef() {
    return ds_def += 25;
  }

  double criDs() {
    return ds_atk = ds_atk * 1.5;
  }
}

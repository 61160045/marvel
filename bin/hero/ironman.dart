mixin ironman {
  double iron_hp = 450;
  double iron_atk = 120;
  double iron_def = 90;

  double getHpIron() {
    return iron_hp;
  }

  String callIron() {
    return 'I am Iron man';
  }

  void ironwasAtk(double botatk) {
    this.iron_hp -= botatk;
  }

  double getIronmanAtk() {
    return iron_atk;
  }

  double checkIronDef(double atk) {
    if (iron_def - atk < 0) {
      return atk - iron_def;
    }
    return 0;
  }

  double ironDef() {
    return iron_def += 10;
  }

  double criIron() {
    return iron_atk = iron_atk * 1.3;
  }
}

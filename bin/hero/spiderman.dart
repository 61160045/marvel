mixin spiderman {
  double sm_hp = 450;
  double sm_atk = 110;
  double sm_def = 90;

  double getHpSpi() {
    return sm_hp;
  }

  String callSpi() {
    return 'hi! I am Spider man.';
  }

  void spiwasAtk(double botatk) {
    this.sm_hp -= botatk;
  }

  double getSpiAtk() {
    return sm_atk;
  }

  double checkSpiDef(double atk) {
    if (sm_def - atk < 0) {
      return atk - sm_def;
    }
    return 0;
  }

  double spiDef() {
    return sm_def += 20;
  }

  double criSpi() {
    return sm_atk = sm_atk * 1.3;
  }
}

mixin blackwidow {
  double bw_hp = 450;
  double bw_atk = 150;
  double bw_def = 120;

  double getHpBlack() {
    return bw_hp;
  }

  String callBlackWi() {
    return 'They are called me Black widow.';
  }

  void blackwasAtk(double botatk) {
    this.bw_hp -= botatk;
  }

  double getBlackWiAtk() {
    return bw_atk;
  }

  double checkBlackDef(double atk) {
    if (bw_def - atk < 0) {
      return atk - bw_def;
    }
    return 0;
  }

  double blackDef() {
    return bw_def += 10;
  }

  double criBlack() {
    return bw_atk = bw_atk * 1.3;
  }
}

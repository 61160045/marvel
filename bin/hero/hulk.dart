mixin hulk {
  double hk_hp = 500;
  double hk_atk = 100;
  double hk_def = 110;

  double getHpHulk() {
    return hk_hp;
  }

  String callHulk() {
    return 'Like a hulk.';
  }

  void hulkwasAtk(double botatk) {
    this.hk_hp -= botatk;
  }

  double getHulkAtk() {
    return hk_atk;
  }

  double checkHulkDef(double atk) {
    if (hk_def - atk < 0) {
      return atk - hk_def;
    }
    return 0;
  }

  double hulkDef() {
    return hk_def += 15;
  }

  double criHulk() {
    return hk_atk = hk_atk * 1.3;
  }
}
